const isPrime = require('./primeNumber-02.js')

const sumPrimeFromArray = (arr) => {
    
    // Filter only the prime number
    const filtered = arr.filter((e)=>{
        return isPrime(e)
    })

    // Sum all the filtered array
    return filtered.reduce((a,b) => a+b ,0)
}

module.exports =  sumPrimeFromArray