const factorialNumber = (num) => {

    let result = 1

    // Iterate up to the param to get the result
    for(let i = 1 ; i <= num; i++){
        result *= i
    }

    return result
}

module.exports = factorialNumber