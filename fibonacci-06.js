const fibonacci = (length) => {
    
    // Initial starter array
    let arr = [0,1];

    // Iterate until array length not more than the param
    while(arr.length <= length - 1){
        
        // Get the last item in array and sum it, and then push the result in array
        arr.push(arr.slice(-2).reduce((a,b)=>a+b))
    }

    // Join the array with comma to get the result as a string "0,1,..."
    return arr.join()
}


module.exports =  fibonacci