
const isPrimeNumber = (num) => {

    // One is not prime
    if(num === 1) return false

    /**
     * By default, all number always can divide by 1 and the number
     * So, I only divide start by 2 up to before than the number
     */

    let i = 2;

    for(i; i < num ; i++){
        
        // If the number can divide by looping value, it's not prime
        if (num%i === 0) return false
    }

    // If there aren't looping value that can divide the number, So the number is a prime
    return true
}

// console.log(isPrimeNumber(11));
module.exports = isPrimeNumber



