const isRepeatDigits = require('../isRepeatDigits-05');

test('05. Check for repeated digits',()=>{
    expect(isRepeatDigits(7)).toBe(false);
    expect(isRepeatDigits(16)).toBe(false);
    expect(isRepeatDigits(55)).toBe(true);
    expect(isRepeatDigits(78)).toBe(false);
    expect(isRepeatDigits(6666)).toBe(true);
})