const factorialNumber = require('../factorialNumbers-04');

test('04. Factorial numbers',()=>{
    expect(factorialNumber(3)).toBe(6);
    expect(factorialNumber(5)).toBe(120);
    expect(factorialNumber(4)).toBe(24)
})