const getUpper5 = require('../greaterThanFive-01.js')

test('01. Number greater than 5', () => { 
    const arrA = [17, 7, 3, 6, 10, 1]
    expect(getUpper5(arrA)).toBe(4)

    const arrB = [5, 2, 190, 6, 68, 1]
    expect(getUpper5(arrB)).toBe(3)

    const arrC = [5, 2, 2, 0, 3, 1]
    expect(getUpper5(arrC)).toBe(0)
})