const isPrimeNumber = require('../primeNumber-02.js')

test('02. Prime number algorithm', () => { 
    expect(isPrimeNumber(5)).toBe(true);
    expect(isPrimeNumber(4)).toBe(false);
    expect(isPrimeNumber(10)).toBe(false);
    expect(isPrimeNumber(2)).toBe(true);
})