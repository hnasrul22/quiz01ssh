const missingNumber = require('../missingNumber-07');

test('07. Missing number game',()=>{
    expect(missingNumber([2,1,4,7])).toBe('3,5,6');
    expect(missingNumber([16,19,13,20])).toBe('14,15,17,18');
    expect(missingNumber([22,28])).toBe('23,24,25,26,27');
})