const fibonacci = require('../fibonacci-06');

test('06. Fibonacci algorithm',()=>{
    expect(fibonacci(3)).toBe('0,1,1')
    expect(fibonacci(5)).toBe('0,1,1,2,3')
    expect(fibonacci(10)).toBe('0,1,1,2,3,5,8,13,21,34')
})