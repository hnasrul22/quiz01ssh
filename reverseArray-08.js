const reverse = (arr) => {
    
    let arrNew = [];

    for(let i = arr.length-1 ; i >= 0 ; i--){
        arrNew = [
            ...arrNew,
            arr[i]
        ]
    }

    return arrNew
}

module.exports = reverse