const getUpper5 = (arr) => {

    // Filtering array to get only greater than 5
    const filtered = arr.filter((e)=> e > 5)

    // Return array length as a number value which greater than 5
    return filtered.length
}

module.exports = getUpper5